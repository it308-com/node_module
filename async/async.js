var async = require('async');
var fs = require('fs');

var arr = [
    {name: 'jack', delay: 200},
    {name: 'mike', delay: 100},
    {name: 'freedown', delay: 300}
];
/////////////////eachSeries////////////////////////
async.eachSeries(
    arr,
    function(item, callback){
        console.log('enter：' + item.name);
        //callback('error');
    },
    function(err){
        if(err){
            console.log('err: ' + err);
        }else{
            //console.log('err called');
        }
        console.log('err called');
    }
);

/////////////////each////////////////////////
/*
// 分析源码用
async.each(
    arr,
    //这里callback是一个only_once包装函数他里面包装的是done,done里面包装了最后的错误回调函数
    function(item, callback){
        console.log('enter: ' + item.name);
        // 经过 only_once 包装的只执行一次的函数
        callback('a', item.name);
        // 执行2次是要报异常的，因为这个callback函数是被only_once包装过的
        //callback('a', item.name);
    },
    function(err){
        console.log('error：' + err);
    }
);
*/

/**
 * each 对一个数组执行同一个异步操作
 */
/*
async.each(
    ['./file/1.txt','./file/2.txt'],
    function(file, callback){
        //console.log('处理文件：' + file);
        //callback();
        //fs.readFile(file, callback);
        fs.readFile(file, function(err, data){
            console.log(data);
        });
    },
    function(err){
        if(err){
            console.log('嗨，一个错误发生了！');
        }else{
            console.log('完成所有工作。');
        }
    }
);
*/

/*
async.each(
    arr,
    function(item, callback){
        console.log('enter: ' + item.name);
        setTimeout(function(){
            console.log('handle: ' + item.name);
            callback(null, item.name);
        }, item.delay);
    },
    function(err){
        console.log('error：' + err);
    }
);
*/

/*
// 如果有一个错误后续的函数会继续执行
async.each(
    arr,
    function(item, callback){
        console.log('enter: ' + item.name);
        if(item.name == 'mike'){
            callback('mike');
        }
    },
    function(err){
        if(err){
            console.log('err: ' + err);
        }
    }
);
*/

/*
// 出错后续的函数将不会被执行
async.eachSeries(
    arr,
    function(item, callback){
        console.log('enter: ' + item.name);
        if(item.name == 'mike'){
            callback('mike');
        }
        //callback();
    },
    function(err){
        if(err){
            console.log('error: ' + err);
        }
    }
);
*/

/*
// 分批执行，第二个参数是每一批的个数，每一批内并行执行，批与批之间按顺序执行
async.eachLimit(
    arr,
    2,
    function(item, callback){
        console.log(item.name);
        callback(null, item.name);
    },
    function(err){
        if(err){
            console.log('error: ' + err);
        }
    }
)
*/

// 如果程序出错同一批继续执行，下一批将不会再执行了
/*
async.eachLimit(
    arr,
    2,
    function(item, callback){
        console.log(item.name);
        if(item.name == 'mike'){
            callback('jack');
        }
    },
    function(err){
        if(err){
            console.log('error: ' + err);
        }
    }
)
*/









